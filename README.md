# gbmorseedit

gbmorseedit is an attempt to write a text editor for classic gameboy
computers (game consoles) that uses morse code to input or output text.


## Compilation

To be able to compile the gbmorseedit source code, you need sdcc
(small device C compiler) and its tools. On Devuan- or Debian- based
GNU/Linux systems, the following command will install all required programs:

    apt install sdcc

After that, you can just run the build.sh script in this repository. It will
generate a gameboy binary that is ready to use with an emulator or real
hardware, in case you have a setup to program gameboy cartidges.


## How to use

Development is still at the beginning, so the program isn't usable yet.
