/*
 *  gbmorseedit - A text editor using morse code for gameboy systems.
 *  Copyright (C) 2020  Moritz Strohm <ncc1988@posteo.de>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "./libgbhw/gbhw.h"




unsigned char setupMorseTone()
{
    //unsigned char* snd_conf = (unsigned char*) 0xff26;
    //*snd_conf = 0b00000000;
    //Silence the other sound channels:
    unsigned char* snd2vol = (unsigned char*) 0xff17;
    *snd2vol = 0b00000000;

    unsigned char* snd1 = (unsigned char*) 0xff11;
    *snd1 = 0b10000000;
    unsigned char* snd1vol = (unsigned char*) 0xff12;
    *snd1vol = 0b00000000;
    unsigned char* freq_lo = (unsigned char*) 0xff13;
    unsigned char* freq_hi = (unsigned char*) 0xff14;
    *freq_lo = 0b10000000;
    *freq_hi = 0b10000111;

    //if (!(*snd_conf & 0x01)) {
    //    //Error while setting up the first sound channel.
    //    return 1;
    //}
    return 0;
}


void startTone()
{
    unsigned char snd1_config = 0b10000000;
    unsigned char volume = 0b11111111;
    unsigned char* snd1 = (unsigned char*) 0xff11;
    *snd1 = snd1_config;
    unsigned char* sndvol = (unsigned char*) 0xff12;
    if (*sndvol != volume) {
        *sndvol = volume;
    }
}


void stopTone()
{
    unsigned char volume = 0b00000000;
    unsigned char* sndvol = (unsigned char*) 0xff12;
    if (*sndvol != volume) {
        *sndvol = volume;
    }
}


void setupTile(unsigned char tile_number, unsigned short* tile_data)
{
    unsigned short* tile_addr = (unsigned short*) first_tile + tile_number * 16;
    //It seems that there is no memcpy function defined, so we have to
    //move the tile data "by hand":
    *tile_addr = tile_data[0];
    tile_addr += 2;
    *tile_addr = tile_data[1];
    tile_addr += 2;
    *tile_addr = tile_data[2];
    tile_addr += 2;
    *tile_addr = tile_data[3];
    tile_addr += 2;
    *tile_addr = tile_data[4];
    tile_addr += 2;
    *tile_addr = tile_data[5];
    tile_addr += 2;
    *tile_addr = tile_data[6];
    tile_addr += 2;
    *tile_addr = tile_data[7];
    tile_addr += 2;
}


void setupBackground()
{
    //Setup two tiles:
    unsigned short* tile_data = (unsigned short*) 0xd000;
    tile_data[0] = 0x0000;
    tile_data[1] = 0x0000;
    tile_data[2] = 0x0000;
    tile_data[3] = 0x0000;
    tile_data[4] = 0x0000;
    tile_data[5] = 0x0000;
    tile_data[6] = 0x0000;
    tile_data[7] = 0x0000;
    setupTile(0, tile_data);
    tile_data[0] = 0xffff;
    tile_data[1] = 0xffff;
    tile_data[2] = 0xffff;
    tile_data[3] = 0xffff;
    tile_data[4] = 0xffff;
    tile_data[5] = 0xffff;
    tile_data[6] = 0xffff;
    tile_data[7] = 0xffff;
    setupTile(1, tile_data);

    //Fill the tile maps:
    unsigned short* tile_map = (unsigned short*) 0x9800;
    while (tile_map < (unsigned short*) 0x9c00) {
        *tile_map = 0x0000;
        tile_map += 2;
    }
    while (tile_map < (unsigned short*) 0xa000) {
        *tile_map = 0x0101;
        tile_map += 2;
    }

    //Select the tile data area:
    unsigned char* lcd = (unsigned char*) 0xff40;
    *lcd = 0b10100001;
}


void toggleDarkMode()
{
    //Switch the selected tile data area:
    unsigned char* lcd = (unsigned char*) 0xff40;
    if (*lcd == 0b10100001) {
        *lcd = 0b11100001;
    } else {
        *lcd = 0b10100001;
    }
}


void showBsod()
{
    //Make everythink black:
    unsigned char* tile_data = (unsigned char*) 0x9000;
    while (tile_data < (unsigned char*) 0x9010) {
        *tile_data = 0x00;
        tile_data++;
    }
    unsigned char* tile_map = (unsigned char*) 0x9800;
    while (tile_map < (unsigned char*) 0x9c00) {
        *tile_map = 0x00;
        tile_map++;
    }
    unsigned char* lcd = (unsigned char*) 0xff40;
    *lcd = 0b10100001;
}


int main()
{
    setupBackground();
    if (setupMorseTone() != 0) {
        //Fatal error.
        showBsod();
        return 1;
        }
    addButtonHandler(BUTTON_UP, 1, startTone);
    addButtonHandler(BUTTON_UP, 0, stopTone);
    addButtonHandler(BUTTON_DOWN, 1, toggleDarkMode);

    while (1) {
        checkButtonStates(0);
        checkButtonStates(1);
        handleButtons();
    }
}
