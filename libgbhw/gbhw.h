/*
 *  This file is part of libgbhw
 *  - A library for interfacing with gameboy hardware
 *  Copyright (C) 2020  Moritz Strohm <ncc1988@posteo.de>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * The definition of NULL for pointers:
 */
#define NULL 0


//Button numbering:
#define BUTTON_RIGHT 0
#define BUTTON_LEFT 1
#define BUTTON_UP 2
#define BUTTON_DOWN 3
#define BUTTON_A 4
#define BUTTON_B 5
#define BUTTON_SELECT 6
#define BUTTON_START 7


/**
 * This function array holds the handlers
 * for each button that is pressed.
 */
void (*button_press_handlers[8])() = {
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
};


/**
 * This function array holds the handlers
 * for each button that is released.
 */
void (*button_release_handlers[8])() = {
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
};



/**
 * The last known states for each button.
 * Each button number corresponds to a bit.
 * With this can be possible to detect a button press
 * or a button release.
 */
unsigned char known_button_states = 0;


/**
 * The buttons that have been pressed.
 */
unsigned char pressed_buttons = 0;


/**
 * The buttons that have been released.
 */
unsigned char released_buttons = 0;


/**
 * A pointer to the first tile in RAM.
 */
unsigned short* first_tile = (unsigned short*) 0x9000;


/**
 * Sets up a button handler for a specific button.
 *
 * @param unsigned char button The button number.
 *
 * @param unsigned char press Whether to handle the button press event (1)
 *     or the release event (0).
 *
 * @param void (*callback)() The callback function that shall be called when
 *     the button event occurs.
 */
void addButtonHandler(unsigned char button, unsigned char press, void (*callback)())
{
    if (press) {
        button_press_handlers[button] = callback;
    } else {
        button_release_handlers[button] = callback;
    }
}


/**
 * Checks the current button states for the two sets of buttons.
 * In case one of the buttons is pressed, the bit representing that button
 * in the pressed_buttons variable is set and the corresponding pressed
 * button handler from the pressed_button_handlers function array is called.
 * Releasing buttons is handled in the same way.
 *
 * @param unsigned char direction_keys Whether to check the direction keys (1)
 *     or the button keys (0).
 */
void checkButtonStates(unsigned char direction_keys) {
    unsigned char* joypad = (unsigned char*)0xff00;
    //Set the joypad register, then read the least four bits
    //from it to get the pressed button.
    if (direction_keys) {
        *joypad = 0b00101111;
        unsigned char state = (*joypad & 0b00001111);
        unsigned char relevant_known_states = (known_button_states & 0b00001111);
        unsigned char changed_states = state ^ relevant_known_states;
        if (!changed_states) {
            //No state change for the direction keys.
            return;
        }
        //We must check all four bits for changes.

        //Right button:
        if ((changed_states & 0b00000001) !=
            (relevant_known_states & 0b00000001)) {
            if (relevant_known_states & 0b00000001) {
                //The right button has been released.
                released_buttons |= 0b00000001;
                pressed_buttons &= 0b11111110;
            } else {
                //The right button has been pressed.
                pressed_buttons |= 0b00000001;
                released_buttons &= 0b11111110;
            }
        }
        //Left button:
        if ((changed_states & 0b00000010) !=
            (relevant_known_states & 0b00000010)) {
            if (relevant_known_states & 0b00000010) {
                //The left button has been released.
                released_buttons |= 0b00000010;
                pressed_buttons &= 0b11111101;
            } else {
                //The left button has been pressed.
                pressed_buttons |= 0b00000010;
                released_buttons &= 0b11111101;
            }
        }
        //Up button:
        if ((changed_states & 0b00000100) !=
            (relevant_known_states & 0b00000100)) {
            if (relevant_known_states & 0b00000100) {
                //The up button has been released.
                released_buttons |= 0b00000100;
                pressed_buttons &= 0b11111011;
            } else {
                //The up button has been pressed.
                pressed_buttons |= 0b00000100;
                released_buttons &= 0b11111011;
            }
        }
        //Down button:
        if ((changed_states & 0b00001000) !=
            (relevant_known_states & 0b00001000)) {
            if (relevant_known_states & 0b00001000) {
                //The down button has been released.
                released_buttons |= 0b00001000;
                pressed_buttons &= 0b11110111;
            } else {
                //The down button has been pressed.
                pressed_buttons |= 0b00001000;
                released_buttons |= 0b11110111;
            }
        }
        //Update the known button states for the direction keys:
        known_button_states &= 0b11110000;
        known_button_states |= state;
    } else {
        *joypad = 0b00011111;
        unsigned char state = ((*joypad & 0b00001111) ) << 4;
        unsigned char relevant_known_states = (known_button_states & 0b11110000);
        unsigned char changed_states = state ^ relevant_known_states;
        if (!changed_states) {
            //No state change for the direction keys.
            return;
        }
        //We must check all four bits for changes.

        //A button:
        if ((changed_states & 0b00010000) !=
            (relevant_known_states & 0b00010000)) {
            if (relevant_known_states & 0b00010000) {
                //The A button has been released.
                released_buttons |= 0b00010000;
                pressed_buttons &= 0b11101111;
            } else {
                //The A button has been pressed.
                pressed_buttons |= 0b00010000;
                released_buttons |= 0b11101111;
            }
        }
        //B button:
        if ((changed_states & 0b00100000) !=
            (relevant_known_states & 0b00100000)) {
            if (relevant_known_states & 0b00100000) {
                //The B button has been released.
                released_buttons |= 0b00100000;
                pressed_buttons &= 0b11011111;
            } else {
                //The B button has been pressed.
                pressed_buttons |= 0b00100000;
                released_buttons |= 0b11011111;
            }
        }
        //Select button:
        if ((changed_states & 0b01000000) !=
            (relevant_known_states & 0b01000000)) {
            if (relevant_known_states & 0b01000000) {
                //The select button has been released.
                released_buttons |= 0b01000000;
                pressed_buttons &= 0b10111111;
            } else {
                //The select button has been pressed.
                pressed_buttons |= 0b01000000;
                released_buttons |= 0b10111111;
            }
        }
        //Start button:
        if ((changed_states & 0b10000000) !=
            (relevant_known_states & 0b10000000)) {
            if (relevant_known_states & 0b10000000) {
                //The start button has been released.
                released_buttons |= 0b10000000;
                pressed_buttons &= 0b01111111;
            } else {
                //The start button has been pressed.
                pressed_buttons |= 0b10000000;
                released_buttons |= 0b01111111;
            }
        }

        //Update the known button states for the buttons:
        known_button_states &= 0b00001111;
        known_button_states |= state;
    }
}


/**
 * Calls the callback functions for each pressed and
 * released button and resets the pressed_buttons and
 * released_buttons variable.
 */
void handleButtons()
{
    if (pressed_buttons) {
        for (unsigned char i = 0; i < 8; i++) {
            if ((pressed_buttons & 0x01) && button_press_handlers[i] != NULL) {
                button_press_handlers[i]();
            }
            pressed_buttons = pressed_buttons >> 1;
        }
    }
    if (released_buttons) {
        for (unsigned char i = 0; i < 8; i++) {
            if ((released_buttons & 0x01) && button_release_handlers[i] != NULL) {
                button_release_handlers[i]();
            }
            released_buttons = released_buttons >> 1;
        }
    }
}
