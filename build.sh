#!/bin/bash


#Program configuration:

# The program name. The binary file will have that name and the extension ".gb".
program_name="gbmorseedit"

# The short program name. It will be stored in the game boy header.
short_program_name="MORSE_ED"

# All source files of the program.
program_files=('gbmorseedit.c')

# Build for gameboy color (1) or the original gameboy (0)?
build_gbc=0


sdcc --version > /dev/null
if [ $? -eq 127 ]
then
    echo "sdcc is not installed! Cannot continue!"
    exit 1
fi

makebin --help 2> /dev/null
if [ $? -eq 127 ]
then
    echo "makebin (from the sdcc package) is not installed! Cannot continue!"
    exit 1
fi


echo "Starting build of the gameboy binary in the subdirectory named \"build\"!"
mkdir -p ./build
if [ $? -ne 0 ]
then
    echo "Cannot create \"build\" directory! Aborting!"
    exit 1
fi

sdcc -mgbz80 -o ./build/ ${program_files[@]}
if [ $? -ne 0 ]
then
    echo "sdcc error! Aborting!"
    exit 1
fi

if [ $build_gbc -ne 0 ]
then
    makebin -yn "$short_program_name" -yc -Z "./build/$program_name.ihx" > "./build/$program_name.gb"
else
    makebin -yn "$short_program_name" -Z "./build/$program_name.ihx" > "./build/$program_name.gb"
fi
if [ $? -ne 0 ]
then
    echo "makebin error! Aborting!"
    exit 1
fi


echo "Build finished! The gameboy binary is located at ./build/$program_name.gb!"
